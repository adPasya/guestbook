<?php

	class dbase 
	{

		private $DB = NULL;

		public function __construct() {

			$this->DB = new PDO("mysql:host=localhost;dbname=guest_book;charset=utf8", 'book_user', '321321');
		}

		public function getAllMessages() {

			$allMessages = $this->DB->query('SELECT * FROM new_table');

			$allMessages->setFetchMode(PDO::FETCH_ASSOC);

			return ($allMessages->fetchAll());

		}

		public function insertMessage($message, $username){

		$STH = $this->DB->prepare('INSERT INTO `new_table` (username, message) values (:username, :message)');
		
		if (empty($username) == TRUE) {
			$username = 'Anonymous';
		};

		$STH->bindValue(':username', htmlentities($username));
		
		$STH->bindValue(':message', htmlentities($message));

		$STH->execute();
		}

		public function getMessage($key) {

			$q = "SELECT * FROM new_table WHERE `key` =".$key;

			$STH = $this->DB->query($q);

			$STH->setFetchMode(PDO::FETCH_ASSOC);
			
			return ($STH->fetchAll());
		}

		public function deleteMessage($key) {
			
			if (is_int($key) == TRUE) {

				$q = "DELETE FROM new_table WHERE `key` =".$key;

				$STH = $this->DB->query($q);

				return ('Success');
			}

			else {
				return ('Fail');
			};
		}
	}

?>